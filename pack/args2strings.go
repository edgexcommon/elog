package pack

import (
	"bytes"
	"encoding"
	"fmt"
	"reflect"
	"strings"
	"sync"
	"unicode/utf8"
)

var hex = "0123456789abcdef"

var bufferPool = sync.Pool{
	New: func() interface{} {
		return &bytes.Buffer{}
	},
}


func getBuffer() *bytes.Buffer {
	return bufferPool.Get().(*bytes.Buffer)
}

func poolBuffer(buf *bytes.Buffer) {
	buf.Reset()
	bufferPool.Put(buf)
}


// NOTE: keep in sync with writeQuotedBytes below.
func writeQuotedString(s string) string {
	buf := getBuffer()
	buf.WriteByte('"')
	start := 0
	for i := 0; i < len(s); {
		if b := s[i]; b < utf8.RuneSelf {
			if 0x20 <= b && b != '\\' && b != '"' {
				i++
				continue
			}
			if start < i {
				buf.WriteString(s[start:i])
			}
			switch b {
			case '\\', '"':
				buf.WriteByte('\\')
				buf.WriteByte(b)
			case '\n':
				buf.WriteByte('\\')
				buf.WriteByte('n')
			case '\r':
				buf.WriteByte('\\')
				buf.WriteByte('r')
			case '\t':
				buf.WriteByte('\\')
				buf.WriteByte('t')
			default:
				// This encodes bytes < 0x20 except for \n, \r, and \t.
				buf.WriteString(`\u00`)
				buf.WriteByte(hex[b>>4])
				buf.WriteByte(hex[b&0xF])
			}
			i++
			start = i
			continue
		}
		c, size := utf8.DecodeRuneInString(s[i:])
		if c == utf8.RuneError {
			if start < i {
				buf.WriteString(s[start:i])
			}
			buf.WriteString(`\ufffd`)
			i += size
			start = i
			continue
		}
		i += size
	}
	if start < len(s) {
		buf.WriteString(s[start:])
	}
	buf.WriteByte('"')
	retstr := string(buf.Bytes())
	poolBuffer(buf)
	return retstr
}

// NOTE: keep in sync with writeQuoteString above.
func writeQuotedBytes(s []byte) string{
	buf := getBuffer()
	buf.WriteByte('"')
	start := 0
	for i := 0; i < len(s); {
		if b := s[i]; b < utf8.RuneSelf {
			if 0x20 <= b && b != '\\' && b != '"' {
				i++
				continue
			}
			if start < i {
				buf.Write(s[start:i])
			}
			switch b {
			case '\\', '"':
				buf.WriteByte('\\')
				buf.WriteByte(b)
			case '\n':
				buf.WriteByte('\\')
				buf.WriteByte('n')
			case '\r':
				buf.WriteByte('\\')
				buf.WriteByte('r')
			case '\t':
				buf.WriteByte('\\')
				buf.WriteByte('t')
			default:
				// This encodes bytes < 0x20 except for \n, \r, and \t.
				buf.WriteString(`\u00`)
				buf.WriteByte(hex[b>>4])
				buf.WriteByte(hex[b&0xF])
			}
			i++
			start = i
			continue
		}
		c, size := utf8.DecodeRune(s[i:])
		if c == utf8.RuneError {
			if start < i {
				buf.Write(s[start:i])
			}
			buf.WriteString(`\ufffd`)
			i += size
			start = i
			continue
		}
		i += size
	}
	if start < len(s) {
		buf.Write(s[start:])
	}
	buf.WriteByte('"')
	retstr := string(buf.Bytes())
	poolBuffer(buf)
	return retstr
}

func needsQuotedValueRune(r rune) bool {
	return r <= ' ' || r == '=' || r == '"' || r == utf8.RuneError
}

func writeBytesValue(value []byte) string {
	if bytes.IndexFunc(value, needsQuotedValueRune) != -1 {
		return writeQuotedBytes(value)
	} else {
		return string(value)
	}
}

func writeStringValue(value string, ok bool) string {
	if ok && value == "null" {
		return `"null"`
	} else if strings.IndexFunc(value, needsQuotedValueRune) != -1 {
		return writeQuotedString(value)
	} else {
		return value
	}
}

// MarshalerError represents an error encountered while marshaling a value.
type MarshalerError struct {
	Type reflect.Type
	Err  error
}

func (e *MarshalerError) Error() string {
	return "error marshaling value of type " + e.Type.String() + ": " + e.Err.Error()
}

func safeMarshal(tm encoding.TextMarshaler) (b []byte, err error) {
	defer func() {
		if panicVal := recover(); panicVal != nil {
			if v := reflect.ValueOf(tm); v.Kind() == reflect.Ptr && v.IsNil() {
				b, err = nil, nil
			} else {
				b, err = nil, fmt.Errorf("panic when marshalling: %s", panicVal)
			}
		}
	}()
	b, err = tm.MarshalText()
	if err != nil {
		return nil, &MarshalerError{
			Type: reflect.TypeOf(tm),
			Err:  err,
		}
	}
	return
}

func safeError(err error) (s string, ok bool) {
	defer func() {
		if panicVal := recover(); panicVal != nil {
			if v := reflect.ValueOf(err); v.Kind() == reflect.Ptr && v.IsNil() {
				s, ok = "null", false
			} else {
				s, ok = fmt.Sprintf("PANIC:%v", panicVal), false
			}
		}
	}()
	s, ok = err.Error(), true
	return
}

func safeString(str fmt.Stringer) (s string, ok bool) {
	defer func() {
		if panicVal := recover(); panicVal != nil {
			if v := reflect.ValueOf(str); v.Kind() == reflect.Ptr && v.IsNil() {
				s, ok = "null", false
			} else {
				s, ok = fmt.Sprintf("PANIC:%v", panicVal), true
			}
		}
	}()
	s, ok = str.String(), true
	return
}

func GetInterfaceStr(inter interface{}) string {
	switch v := inter.(type) {
	case nil:
		return writeBytesValue([]byte("null"))
	case string:
		return writeStringValue(v,true)
	case encoding.TextMarshaler:
		vb, err := safeMarshal(v)
		if err != nil {
			return ""
		}
		if vb == nil {
			vb = []byte("null")
		}
		return writeBytesValue(vb)
	case error:
		se, ok := safeError(v)
		return writeStringValue(se, ok)
	case fmt.Stringer:
		ss, ok := safeString(v)
		return writeStringValue(ss, ok)
	default:
		rvalue := reflect.ValueOf(inter)
		switch rvalue.Kind() {
		case reflect.Array, reflect.Chan, reflect.Func, reflect.Map, reflect.Slice, reflect.Struct:
			return ""
		case reflect.Ptr:
			if rvalue.IsNil() {
				return writeBytesValue([]byte("null"))
			}
			return GetInterfaceStr(rvalue.Elem().Interface())
		}
		return writeStringValue(fmt.Sprint(v), true)
	}
}