package elog

import (
	"encoding/json"
	"os"
)

type elogModule struct {
	Id 			int
	Level 		Priority
	Status 		bool
}

var elogCfg 	map[int]elogModule

func initCfg()  {
	fileName := "/etc/crypt/iot_conf/elog/elog.level"
	fi,err := os.Open(fileName)
	if err != nil{
		return
	}

	defer func() {
		err := fi.Close()
		if err == nil {
			return
		}
	}()

	stat,err := fi.Stat()
	if err != nil{
		return
	}
	size := stat.Size()
	datab := make([]byte,size)

	_,err = fi.Read(datab)
	if err != nil{
		return
	}
	var tmpelogs []elogModule
	_=json.Unmarshal(datab, &tmpelogs)
	elogCfg = make(map[int]elogModule)
	for _,elogi := range tmpelogs{
		elogCfg[elogi.Id] = elogi
	}
}

func checkElog(level Priority,module int) bool {
	if elogCfg == nil{
		return true
	}
	elogi,err := elogCfg[module]
	if err == false{
		return true
	}
	if level <= elogi.Level{
		return true
	}

	return false
}
