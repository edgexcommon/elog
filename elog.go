package elog

import (
	"encoding/json"
	"fmt"
	"gitee.com/edgexcommon/elog/log/syslog"
	"gitee.com/edgexcommon/elog/pack"
	"net/http"
	"sort"
)

type Priority int
const (
	// Severity.

	// From /usr/include/sys/syslog.h.
	// These are the same on Linux, BSD, and OS X.
	LOG_EMERG Priority = iota
	LOG_ALERT
	LOG_CRIT
	LOG_ERR
	LOG_WARNING
	LOG_NOTICE
	LOG_INFO
	LOG_DEBUG
)

type Args map[string]interface{}

func Log(level Priority,module int,msg string,args Args)  {
	var tmpl syslog.Priority
	isOk := checkElog(level,module)
	if isOk == false{
		return
	}

	tmpl = syslog.Priority(module << 3)
	tmpl |= syslog.Priority(level)
	strmsg := ""

	keys := make([]string, 0)
	for k, _ := range args {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		strmsg += k + "="
		strmsg += pack.GetInterfaceStr(args[k]) + " "
	}
	
	strmsg += "msg=" + pack.GetInterfaceStr(msg)
	if len(strmsg) > 1024{
		strmsg = strmsg[:1024]
	}
	_,err := slog.WriteAndRetry(tmpl,strmsg)
	if err != nil{
		fmt.Printf("write syslogd error msg:%s\n",strmsg)
	}
}

var (
	slog * syslog.Writer
)

func ElogInit(name string)  {
	initCfg()
	sysLog, _ := syslog.Dial("", "",syslog.LOG_DEBUG, name)
	slog = sysLog
}

func ElogGetHandler() http.HandlerFunc {
	return func(w http.ResponseWriter, request *http.Request) {
		initCfg()
		tmpResult := make(map[string]interface{})
		tmpResult["ERRON"] = 0
		tmpResult["ERRMSG"] = "无错误"

		w.Header().Add("Content-Type", "application/json")

		enc := json.NewEncoder(w)
		enc.Encode(tmpResult)
		return
	}
}